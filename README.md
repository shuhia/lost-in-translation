<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">Lost In Translation</a>
      <ul>
      <li><a href="#component-feature-tree">Component Feature Tree</a></li>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>
 
<!-- About the project -->

## Lost In Translation

Link to demo: [lost-in-translation](https://shuhia-lost-in-translation.herokuapp.com/)

[Requirements doc](./specification.pdf)

### Component Feature Tree

<details>
<summary>Component feature tree</summary>
[Link to figma](https://www.figma.com/file/Z3fYvYGHwnqXayBpeE4W5l/lost-in-translation%3A-Component-Tree?node-id=0%3A1)
</details>

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

- ReactJS
- Material UI

Dependencies are listed in [package.json](./package.json)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

### Prerequisites

[VSCode](https://code.visualstudio.com/) or any other editor.
[NodeJS](https://nodejs.org/en/)

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/shuhia/lost-in-translation.git
   ```
2. Enter the cloned project folder
   ```sh
   cd lost-in-translation
   ```
3. Install NPM packages
   ```sh
   npm install
   ```
4. Run the app
   ```js
   npm start
   ```

<p align="right">(<a href="#top">back to top</a>)</p>

## Usage

How to use the application

1. Fill in a username and press "Sign in"
2. In the navbar on the top of the page, you can press the avatar och navigate to the different pages of the application.
3. In the translate page, you can, as a user, enter text you wish to be translated.
4. Press the translate button and you will get a series of letters in sign-language form.
5. You can enter your own profile (via the avatar on the navbar) to see your 10 latest translations you have entered.
6. If you wish, you can delete the records of the latest words and see if there is more words you have translated.
7. You log out of the application by hitting the "logout" button via the avatar on the navbar.

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

[Alex On](https://www.linkedin.com/in/alex-on-0a08b8107/)

[Sebastian Börjesson](https://www.linkedin.com/in/sebastian-b%C3%B6rjesson-346042151/)

<p align="right">(<a href="#top">back to top</a>)</p>
