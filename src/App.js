import React from "react";
import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Start from "./components/pages/Start";
import Profile from "./components/pages/Profile";
import Translate from "./components/pages/Translate";
import NavBar from "./components/navbar/NavBar";
import Authenticator from "./components/login/Authenticator";

function App() {
  return (
    <Router>
      <header>
        <NavBar></NavBar>
      </header>
      <Routes>
        <Route path="/" element={<Start />} />

        <Route
          path="/translate"
          element={
            <Authenticator>
              <Translate />
            </Authenticator>
          }
        />
        <Route
          path="/profile"
          element={
            <Authenticator>
              <Profile />
            </Authenticator>
          }
        />
      </Routes>
    </Router>
  );
}

export default App;
