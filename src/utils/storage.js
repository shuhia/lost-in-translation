var CryptoJS = require("crypto-js");
const secretKey = process.env.REACT_APP_SECRET_KEY;

/**
 * Encrypts message with AES with message and secretkey
 * @param {String} message - the message that you want to encrypt
 * @returns {String} returns encrypted message
 */
function encrypt(message) {
  const chipertext = CryptoJS.AES.encrypt(message, secretKey).toString();
  return chipertext;
}

/**
 * Decrypts ciphertext with AES
 * @param {String} ciphertext - encrypted message
 * @returns returns decrypted message
 */
function decrypt(ciphertext) {
  var bytes = CryptoJS.AES.decrypt(ciphertext, secretKey);
  var originalText = bytes.toString(CryptoJS.enc.Utf8);
  return originalText;
}

export const storageSave = (key, value) => {
  // encrypt value
  const encrypted = encrypt(JSON.stringify(value));
  localStorage.setItem(key, encrypted);
};

export const storageRead = (key) => {
  const value = localStorage.getItem(key);
  if (value) {
    return JSON.parse(decrypt(value));
  }

  return null;
};

export const storageRemoveItem = (key) => {
  return localStorage.removeItem(key);
};

export const storageClear = () => {
  return localStorage.clear();
};
