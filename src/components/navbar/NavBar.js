import React from "react";
import { useNavigate } from "react-router-dom";
import { useContext, useState } from "react";
import { UserContext } from "../../context/UserContext";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import logo from "./Logo.png";
/**
 * Navbar for the entire app
 */
function NavBar(props) {
  const [user, dispatch] = useContext(UserContext);
  const [anchorElUser, setAnchorElUser] = useState(null);
  const [anchorElNav, setAnchorElNav] = useState(null);
  const navigate = useNavigate();

  // Handlers for the submenu on the avatar
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };
  // Closes the user menu
  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  // Open nav menu
  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };

  // Close nav menu
  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  // Navigates to translate page and closes user menu
  const routeToTranslate = () => {
    navigate("/translate");
    handleCloseUserMenu();
  };

  // Navigates to profile page and closes user menu
  const routeToProfile = () => {
    navigate("/profile");
    handleCloseUserMenu();
  };

  // Logouts user and closes user menu
  const handleLogout = () => {
    dispatch({ type: "LOGOUT" });
    handleCloseUserMenu();
  };
  return (
    <AppBar position="static" sx={{ backgroundColor: "#FFC75F" }}>
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Avatar id="logo" src={logo} />
          <Typography
            variant="h5"
            noWrap
            component="div"
            sx={{
              mr: 2,
              display: { xs: "none", md: "flex" },
              fontFamily: "Love Ya Like A Sister",
            }}
          >
            Lost in Translations
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "flex" } }}>
            <Button
              onClick={handleCloseNavMenu}
              sx={{ my: 2, color: "white", display: "block" }}
            />
          </Box>
          {/* If the user exists, display the avatar and it's submenu */}
          {user !== null && (
            <Box sx={{ flexGrow: 0 }}>
              <Tooltip title="Open settings">
                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                  <Avatar sx={{ backgroundColor: "#E7B355" }} />
                </IconButton>
              </Tooltip>
              <Menu
                sx={{ mt: "45px" }}
                anchorEl={anchorElUser}
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                keepMounted
                transformOrigin={{ vertical: "top", horizontal: "right" }}
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu}
              >
                <MenuItem onClick={routeToTranslate}>
                  <Typography textAlign="center">Translate</Typography>
                </MenuItem>
                <MenuItem onClick={routeToProfile}>
                  <Typography textAlign="center">Profile</Typography>
                </MenuItem>
                <MenuItem onClick={handleLogout}>
                  <Typography textAlign="center">Logout</Typography>
                </MenuItem>
              </Menu>
            </Box>
          )}
        </Toolbar>
      </Container>
    </AppBar>
  );
}

export default NavBar;
