import React from "react";
import { Box, TextField } from "@mui/material/";
import { Button, FormControl } from "@mui/material/";
import { colors } from "../../assets/colors";

/**
 * Renders a form to collect text from user
 * @param {Object} props - requires state, setState and a event handler
 */

function TranslationForm({ letters, setLetters, handleTranslate }) {
  return (
    <form onSubmit={handleTranslate}>
      <FormControl>
        <Box display="flex">
          <TextField
            id="outlined-basic"
            label="Text"
            variant="outlined"
            type="text"
            value={letters}
            minLength={0}
            maxLength={40}
            required
            onChange={(e) => {
              setLetters(e.target.value);
            }}
            multiline
            placeholder={"max length is 40"}
          />
          <Box>
            <Button
              size="large"
              type="submit"
              variant="contained"
              sx={{ backgroundColor: colors.PURPLE }}
            >
              Translate
            </Button>
          </Box>
        </Box>
      </FormControl>
    </form>
  );
}

export default TranslationForm;
