import React from "react";
import Sign from "../sign/Sign";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { Box } from "@mui/material";
import { colors } from "../../assets/colors";
/**
 * Renders a card that displays signs
 * @param {String} lettersToTranslate
 */
function TranslationCard({ lettersToTranslate }) {
  const signs = createSignsFrom(lettersToTranslate);

  // Creates Sign elements with letters. Requires Sign component.
  function createSignsFrom(letters) {
    return lettersToTranslate
      .toLowerCase()
      .split("")
      .filter((char) => "a" <= char && char <= "z")
      .map((letter, index) => <Sign letter={letter} key={index}></Sign>);
  }
  return (
    <Card>
      <CardContent>
        <Box sx={{ display: "flex", flexWrap: "Wrap" }}>{signs}</Box>
      </CardContent>
      <Box
        sx={{
          display: "flex",
          background: colors.PURPLE,
          textAlign: "left",
        }}
      >
        <Box
          sx={{
            padding: "5px",
            background: colors.LIGHT_GREY,
            margin: "10px",
            borderRadius: "20px",
            fontSize: "10px",
          }}
        >
          Translation
        </Box>
      </Box>
    </Card>
  );
}
export default TranslationCard;
