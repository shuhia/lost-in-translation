import { Paper } from "@mui/material";
// Renders a sign from a letter
// Requires a sign images named `${letters}.png` where letters are between a to z stored in folder individual-signs

export default function Sign({ letter }) {
  const path = "./individual-signs/";
  const src = require(path + letter + ".png");
  return (
    <Paper elevation={2}>
      <img style={{ height: "5ch", width: "5ch" }} src={src} alt={letter}></img>
    </Paper>
  );
}
