import Alert from '@mui/material/Alert';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { useForm } from "react-hook-form";
import { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { loginUser } from "../../api/user";
import { UserContext } from "../../context/UserContext";

// Set the username as required for logging in
const userNameConfig = {
  required: true,
};

const LoginForm = () => {
  // Hooks
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const navigate = useNavigate();
  const [user, dispatch] = useContext(UserContext);
  
  // States
  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);

  // Side effects
  useEffect(() => {
    if (user) {
      navigate("/translate");
    }
  }, [user]);

  // Event handlers
  const onSubmit = async ({ username }) => {
    // Disable the sign button, preventing multiple submit calls
    setLoading(true);
    // Trigger the api call for logging in user
    const [error, user] = await loginUser(username);
    if (error !== null) {
      console.log("something went wrong");
    }
    // If the user is returned, dispatch the event in the context to set the user
    if (user !== null) {
      dispatch({ type: "LOGIN", payload: user });
    }
    setLoading(false);
  };

  // Error handling for not typing in a valid input
  const errorMessage = (() => {
    // If no errors, nothing happens
    if(!errors.username) {
        return null
    }
    // If the input (is required) is not valid, return a alert
    if (errors.username.type === 'required') {
        return <Alert variant="outlined" severity="error">Username is required!</Alert>
    }
  })()

  return (
    <>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: '#845EC2' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <Box component="form" onSubmit={handleSubmit(onSubmit)} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              label="Username"
              name="username"
              autoComplete="username"
              autoFocus
              {...register("username", userNameConfig)}
            />
            
            <Button
              type="submit"
              fullWidth
              variant="contained"
              disabled={loading}
              sx={{ mt: 3, mb: 2, bgcolor: "#845EC2" }}
            >
              Sign In
            </Button>
            {errorMessage}
            
          </Box>
        </Box>
      </Container>
    </>
  );
};


export default LoginForm;
