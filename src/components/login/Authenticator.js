import React, { useContext, useEffect } from "react";
import { UserContext } from "../../context/UserContext";
import { useNavigate } from "react-router-dom";

// Used to protect routes from unauthenticated users
// Renders children when user is authenticated
// Else redirects user to login page

function Authenticator(props) {
  const [user, dispatch] = useContext(UserContext);
  let navigate = useNavigate();
  // When this page has finished loading redirect user to login page if not authenticated
  useEffect(() => {
    if (user === null) navigate("/");
  }, [user]);
  // If user is authenticated render page else redirect to start page
  if (user !== null) return <>{props.children}</>;
  else {
    return <>Error</>;
  }
}

export default Authenticator;
