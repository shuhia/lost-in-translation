import React, { useContext } from "react";
import { UserContext } from "../../context/UserContext";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";

// Displays the 10 latest translations that user has entered on Translate Page
// Can be used to remove the 10 latest translations

function Profile() {
  const [user, dispatch] = useContext(UserContext);
  const translations = user.translations.filter(
    (translation) => !translation.isDeleted
  );
  // Marks Translations as deleted
  const handleDelete = () => {
    dispatch({
      type: "MARK_FOR_DELETE_TRANSLATIONS",
      payload: translations.slice(0, 10),
    });
  };

  // Creates translation elements that display translation text
  const translationElements = translations
    ?.filter((translation) => !translation.isDeleted)
    .slice(0, 10)
    .map((translation, index) => {
      return <div className="text">{index + 1 + "." + translation.text}</div>;
    });

  return (
    <Container sx={{ textAlign: "center", marginTop: 3 }}>
      <Card sx={{ minWidth: 275 }}>
        <CardContent>
          <h1 className="heading">Translations</h1>
          <div>{translationElements}</div>
        </CardContent>
        <CardActions>
          <Button
            sx={{ margin: "auto" }}
            variant="contained"
            onClick={handleDelete}
            color="error"
          >
            Remove
          </Button>
        </CardActions>
      </Card>
    </Container>
  );
}

export default Profile;
