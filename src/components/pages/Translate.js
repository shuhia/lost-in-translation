import React, { useState, useContext } from "react";
import { UserContext } from "../../context/UserContext";
import { nanoid } from "nanoid";
import { Box, Card, CardContent } from "@mui/material/";
import TranslationCard from "../translation/TranslationCard";
import TranslationForm from "../translation/TranslationForm";
import Container from "@mui/material/Container";

// Translate Page
// Displays a Card with TranslationForm and TranslationCard

function Translate() {
  const [user, dispatch] = useContext(UserContext);
  const [letters, setLetters] = useState("");
  const [lettersToTranslate, setLettersToTranslate] = useState("");

  const handleTranslate = (e) => {
    e.preventDefault();
    setLettersToTranslate(letters);
    saveText(letters);
    // Reset input field
    setLetters("");
  };

  const saveText = (translation) => {
    function createTranslationObject() {
      return { id: nanoid(), text: translation, isDeleted: false };
    }
    dispatch({ type: "ADD_TRANSLATION", payload: createTranslationObject() });
  };
  return (
    <Container sx={{ textAlign: "center", marginTop: 3 }}>
      <Card>
        <CardContent>
          <h1 className="heading">Translate</h1>
          <TranslationForm
            letters={letters}
            setLetters={setLetters}
            handleTranslate={handleTranslate}
          ></TranslationForm>
          <TranslationCard
            lettersToTranslate={lettersToTranslate}
          ></TranslationCard>
        </CardContent>
      </Card>
    </Container>
  );
}

export default Translate;
