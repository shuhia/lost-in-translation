export const colors = {
  YELLOW: "#FFC75F",
  YELLOW_DARK: "#E7B355",
  PURPLE: "#845EC2",
  LIGHT_GREY: "#EFEFEF", // (Inputs)
  GREY: "#969696", // (Text)
};
