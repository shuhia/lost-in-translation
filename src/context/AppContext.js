import React from "react";
import UserProvider from "./UserContext";

// Used to add all providers to Index component

const AppContext = ({ children }) => {
  return <UserProvider>{children}</UserProvider>;
};

export default AppContext;
