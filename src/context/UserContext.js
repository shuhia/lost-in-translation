import React, { createContext, useEffect, useReducer, useContext } from "react";
import { updateUser } from "../api/user";
import { storageRead, storageSave, storageClear } from "../utils/storage";

export const UserContext = createContext(null);

function UserContextProvider(props) {
  const reducers = (user, action) => {
    switch (action.type) {
      case "ADD_TRANSLATION": {
        const updatedUser = {
          ...user,
          translations: [action.payload, ...user.translations],
        };
        return updatedUser;
      }
      // Marks all translations for deletion
      case "MARK_FOR_DELETE_TRANSLATIONS": {
        const translations = action.payload;
        return {
          ...user,
          translations: user.translations.map((prevTranslation) => {
            const id = prevTranslation.id;
            if (translations.find((translation) => translation.id === id)) {
              return { ...prevTranslation, isDeleted: true };
            } else return prevTranslation;
          }),
        };
      }
      // Deletes all translations
      case "DELETE_ALL_TRANSLATIONS": {
        return { ...user, translations: [] };
      }
      case "LOGIN": {
        const user = action.payload;
        return user;
      }
      case "LOGOUT": {
        storageClear();
        return null;
      }
      default:
        return { ...user };
    }
  };
  const [user, dispatch] = useReducer(reducers, storageRead("user"));
  useEffect(() => {
    if (user) {
      updateUser(user.id, user.translations);
      storageSave("user", user);
    }
  }, [user]);
  return (
    <UserContext.Provider value={[user, dispatch]}>
      {props.children}
    </UserContext.Provider>
  );
}

export default UserContextProvider;
