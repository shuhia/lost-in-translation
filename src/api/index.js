// API KEY
const apiKey = process.env.REACT_APP_API_KEY;

/**
 * Creates a header object with apiKey and Content Type
 * Used for API requests that requires authentication
 * @returns a header object
 */
export const createHeaders = () => {
  return {
    "X-API-Key": apiKey,
    "Content-Type": "application/json",
  };
};
