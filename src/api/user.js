import { createHeaders } from "./index";

// BASE URL for API

const apiUrl = process.env.REACT_APP_API_URL;
/**
 * Check if user exists in database
 * @param {String} username
 * @return {Promise<any[]>}
 */
const checkForUser = async (username) => {
  try {
    const response = await fetch(`${apiUrl}?username=${username}`);
    if (!response.ok) {
      throw new Error("Could not complete request");
    }

    const data = await response.json();
    return [null, data];
  } catch (error) {
    return [error.message, []];
  }
};

/**
 * Creates a user in the API with username
 * @param {String} username
 * @return {Promise<any[]>} Returns a promise with array that contains two values. First value can be null which identicates no error. Second can be a user object or empty array.
 */

const createUser = async (username) => {
  try {
    const response = await fetch(apiUrl, {
      method: "POST",
      headers: createHeaders(),
      body: JSON.stringify({
        username,
        translations: [],
      }),
    });
    if (!response.ok) {
      throw new Error("Could not create user with username" + username);
    }

    const data = await response.json();
    return [null, data];
  } catch (error) {
    return [error.message, []];
  }
};

/**
 * Logins user with username
 * @param {String} username
 * @return {Promise<any[]>} Returns a promise with array that contains two values. First value can be null which identicates no error. Second can be a user object or empty array.
 */
export const loginUser = async (username) => {
  // Check if user already exists
  const [checkError, user] = await checkForUser(username);

  if (checkError !== null) {
    return [checkError, null];
  }

  // If the user exists, add user to the state
  if (user.length > 0) {
    return [null, user.pop()];
  }

  // If not, create a new and set it to the state
  return await createUser(username);
};

/**
 * Updates user with a new translation object by specifying userId
 * @param {String} userId - id of user
 * @param {Object} translations - translations object
 * @return {Promise<any[]>} - returns a promise with a new user object or undefined
 */
export const updateUser = async (
  // Arguments for how the user should be updated
  userId,
  translations
) => {
  return fetch(`${apiUrl}/${userId}`, {
    method: "PATCH", // NB: Set method to PATCH
    headers: createHeaders(),
    body: JSON.stringify({
      // Provide new translations to add to user with the current id
      translations: translations,
    }),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Could not update translations history");
      }
      return response.json();
    })
    .then((updatedUser) => {
      return updatedUser;
    })
    .catch((error) => {
      console.log(error);
    });
};
